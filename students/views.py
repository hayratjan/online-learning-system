import os.path

from django.shortcuts import render, HttpResponse, HttpResponseRedirect
from login.models import *


# Create your views here.
def index(request):
    name = request.session['name']
    a = User.objects.get(name=name)

    s = a.type

    course = Course2.objects.filter(students=a)
    return render(request, "studentes/stu_index.html", {"s": s, "course": course})


def kecheng(request, id):
    try:

        cource = Course.objects.get(id=id, is_active=True)

    except Exception as e:
        print("cource_edit:", e)
        msg = "没有此课程！"
        return render(request, 'teachers/prompt.html', {"msg": msg})

    chapter_id = request.GET.get('chapter')
    section_id = request.GET.get('section')

    chapter = Chapter.objects.filter(name__id=id, is_active=True)
    section = Section.objects.filter(name__name_id=id, is_active=True)

    if section_id:
        try:
            section_id = Section.objects.get(id=section_id, is_active=True)
            message = Message.objects.filter(name_id=section_id, is_active=True)
        except Exception as e:
            print("cource_edit section_id：", e)
            msg = "没有此课程！"
            # return render(request, 'teachers/prompt.html', {"msg": msg})

        return render(request, 'studentes/stu_class.html',
                      {"cource": cource, "chapter": chapter, "section": section, "id": id, "section_id": section_id,
                       "message": message})

    return render(request, 'studentes/stu_class.html',
                  {"cource": cource, "chapter": chapter, "section": section, "id": id})


def kecheng_add(request, id):
    try:
        cource = Course.objects.get(id=id)
    except Exception as e:
        print("student_kecheng_add:", e)
        msg = "没有此课程！"
        return render(request, 'studentes/prompt.html', {"msg": msg})
    if request.method == 'GET':
        name=request.session.get('name')
        try:

            ss = Course2.objects.filter(course_id=cource,students_id=User.objects.get(name=name),is_active=True)
        except Exception as e:
            print("students_kecheng_add",e)
        if ss:
            return HttpResponseRedirect('/students/kecheng/' + str(id))
        return render(request, 'studentes/cource_add.html', {"cource": cource})
    if request.method == 'POST':
        user = request.session.get('name')
        try:
            user = User.objects.get(name=user)
            ss = Course2.objects.filter(students=user, course=cource)
        except Exception as e:
            print("students_kecheng_add:", e)
        if ss:
            msg = "你已经参加了这门课程！"
            return render(request, 'studentes/prompt.html', {"msg": msg})

        course2 = Course2.objects.create(course=cource, students=user)
        if course2:
            return HttpResponseRedirect('/students/kecheng/' + str(id))
        else:
            msg = "添加失败！"
            return render(request, 'studentes/prompt.html', {"msg": msg})


def exercises(request, id):
    try:
        user = request.session.get('name')
        user = User.objects.get(name=user)
        cource = Course.objects.get(id=id)
        exercise = Exercises.objects.filter(is_active=True, course_id=id)
        exercise2 = Exercises2.objects.filter(is_active=True, course2__course_id=id, user=user)
    except Exception as e:
        print("student_exercises:", e)
        msg = "没有此课程！"
        return render(request, 'studentes/prompt.html', {"msg": msg})
    if request.method == 'GET':
        a = []
        # try:
        #     for i in exercise:
        #         b = []
        #         b.append(i)
        #         exe = Exercises2.objects.get(is_active=True, course2__course_id=id, user=user, exercises_id=i.id)
        #         b.append(exe)
        #         a.append(b)
        # except Exception as e:
        #     print("stduents_exercises:", e)

        return render(request, 'studentes/exercises_index.html', {"exercise": exercise, "exercise2": exercise2, "a": a})


def exercises_start(request, id, exercises_id):
    try:
        cource = Course.objects.get(is_active=True, id=id)
        exercise = Exercises.objects.get(is_active=True, id=exercises_id)
        answer = Answer.objects.filter(is_active=True, exercises_id=exercises_id)

    except Exception as e:
        print("student_exercises_start:", e)
        msg = "没有此练习题！"
        return render(request, 'studentes/prompt.html', {"msg": msg})
    if request.method == 'GET':
        return render(request, 'studentes/exercises_start.html',
                      {"exercise": exercise, "answer": answer, "course": cource})


def calGrade(request, id, exercises_id):
    user = request.session.get('name')
    user = User.objects.get(name=user)
    try:

        exercise = Exercises.objects.get(is_active=True, id=exercises_id)
        answer = Answer.objects.filter(is_active=True, exercises_id=exercises_id)
        cource2 = Course2.objects.get(course_id=id, students_id=user.id)

    except Exception as e:
        print("student_calgrade:", e)
        msg = "没有此练习题！"
        return render(request, 'studentes/prompt.html', {"msg": msg})

    if request.method == 'POST':
        myGrade = 0
        for i in answer:
            stu = request.POST[str(i.id)]
            # print(i.id, ":", exercise.id, ":", exercise.exercises, "+", stu)
            if i.correctAnswer == int(stu):
                myGrade = myGrade + 1
            ans = Answer2.objects.create(user=user, exercises=exercise, stuAnswer=stu,
                                         answer=Answer.objects.get(id=i.id))
        grade = myGrade / answer.count() * 100
        grade = round(grade, 2)
        ex = Exercises2.objects.create(user=user, exercises=exercise, course2=cource2, grade=grade, )

    return HttpResponseRedirect('/students/exercises/' + str(id))


# 留言
def cource_section_msg(request, id, chapter_id, section_id):
    try:
        cource = Course.objects.get(id=id)
        chapter = Chapter.objects.get(id=chapter_id)
    except Exception as e:
        print("cource_edit:", e)
        msg = "没有此课程或目录！"
    if request.method == 'GET':
        msg = request.GET.get('msg')
        user = request.session.get('name')
        ss = Message.objects.create(message=msg, user=User.objects.get(name=user),
                                    name=Section.objects.get(id=section_id))
        return HttpResponseRedirect('/students/kecheng/' + str(id))


def showGrade(request, id, exercises_id):
    user = request.session.get('name')
    user = User.objects.get(name=user)
    try:

        exercise = Exercises.objects.get(is_active=True, id=exercises_id)
        answer = Answer.objects.filter(is_active=True, exercises_id=exercises_id)
        cource2 = Course2.objects.get(course_id=id, students_id=user.id)
        ansewer2 = Answer2.objects.filter(user__name=user, exercises_id=exercises_id).values('stuAnswer')

    except Exception as e:
        print("student_calgrade:", e)
        msg = "没有此练习题！"
        return render(request, 'studentes/prompt.html', {"msg": msg})
    if request.method == 'GET':
        return render(request, 'studentes/showGrade.html',
                      {"exercise": exercise, "answer": answer, "course": cource2, "answer2": ansewer2})

