from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name="students_index"),
    path('kecheng/<int:id>', views.kecheng, name="students_kecheng"),
    path('kecheng_add/<int:id>', views.kecheng_add, name="students_kecheng_add"),
    path('cource_section_msg/<int:id>/<int:chapter_id>/<int:section_id>', views.cource_section_msg,
         name="cource_section_msg_students"),  # 课程评论留言
    path('exercises/<int:id>', views.exercises, name="students_exercises"),
    path('exercises_start/<int:id>/<int:exercises_id>', views.exercises_start, name="students_exercises_start"),
    path('calGrade/<int:id>/<int:exercises_id>', views.calGrade, name="students_calGrade"),
    path('showGrade/<int:id>/<int:exercises_id>', views.showGrade, name="students_showGrade"),

]
