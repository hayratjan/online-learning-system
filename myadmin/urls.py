from django.urls import path, include
from . import views

urlpatterns = [
    # ——————————————————————  用户  ————————————————————————
    path('', views.index, name='myadmin_index'),
    path('user/<int:pIndex>', views.user, name="myadmin_user"),
    path('user_edit/<int:id>', views.user_edit, name="myadmin_user_edit"),
    path('user_del/<int:id>', views.user_del, name="myadmin_user_del"),

    # ——————————————————————  审核教师  ————————————————————————
    path('teachers_audit/', views.teachers_audit, name="myadmin_teachers_audit"),
    path('teachers_audit_id/<int:id>', views.teachers_audit_id, name="myadmin_teachers_audit_id"),
    # ——————————————————————  课程  ————————————————————————
    path('cource/', views.myadmin_cource, name="myadmin_cource"),
    path('cource_del/<int:id>', views.myadmin_cource_del, name="myadmin_cource_del"),
    path('cource_audit/', views.myadmin_cource_audit, name="myadmin_cource_audit"),  # 审核首页
    path('cource_audit/<int:id>', views.myadmin_cource_audit_id, name="myadmin_cource_audit_id"),  # 审核
    path('cource2/', views.myadmin_cource2, name="myadmin_cource2"),  # 选课
]
