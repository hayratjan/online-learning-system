from django.core.paginator import Paginator
from django.db.models import Q
from django.shortcuts import render, HttpResponse, HttpResponseRedirect
from login.models import *


# Create your views here.
def index(request):
    return render(request, "myadmin/myadmin_index.html")


def user(request, pIndex=1):
    user = User.objects.filter().order_by()  # 班级过滤器
    mywhere = []
    # 获取并判断搜索
    kw = request.GET.get("keyword", None)
    if kw:
        stu_list = user.filter(Q(phone__contains=kw) | Q(name__contains=kw))
        mywhere.append('keyword' + kw)

    # 执行分页处理
    pIndex = int(pIndex)
    page = Paginator(user, 10)  # 以每页9条数据分页
    maxpagex = page.num_pages  # 获取最大页数
    # 判断当前页是否越界
    if pIndex > maxpagex:
        pIndex = maxpagex
    if pIndex < 1:
        pIndex = 1

    user = page.page(pIndex)  # 获取当前页数据
    plist = page.page_range  # 获取页码表信息
    context = {"user": user, "plist": plist, "pIndex": pIndex, "max_pages": maxpagex, 'mywehere': mywhere}
    return render(request, 'myadmin/user.html', context)


def user_edit(request, id):
    try:
        ss = User.objects.get(id=id)
    except Exception as e:
        print(e)
        msg = '没有此用户！！'
        return render(request, 'myadmin/prompt.html', {"msg": msg})
    if request.method == 'GET':
        return render(request, 'myadmin/user_edit.html', {"user": ss})
    elif request.method == 'POST':

        """
        :type
        email
        phone
        photo
        teacher_id
        teacher_name
        
        """
        type = request.POST['type']
        phone = request.POST['phone']
        email = request.POST['email']
        ss.type = type
        ss.phone = phone
        ss.email = email
        ss.save()
        print("post成功！")
        msg = '修改成功！'

        return render(request, 'myadmin/prompt.html', {"msg": msg})


def user_del(request, id):
    try:
        ss = User.objects.get(id=id)
    except Exception as e:
        print(e)
        msg = '没有此用户！！'
        return render(request, 'myadmin/prompt.html', {"msg": msg})
    ss.type = 9
    ss.save()
    msg = '删除成功！'
    return render(request, 'myadmin/prompt.html', {"msg": msg})


def teachers_audit(request):
    if request.method == 'GET':
        ss = User.objects.filter(type=4)
        return render(request, 'myadmin/teachers_audit.html', {"user": ss})


def teachers_audit_id(request, id):
    try:
        ss = User.objects.get(id=id)
    except Exception as e:
        print(e)
        msg = '没有此用户！！'
        return render(request, 'myadmin/prompt.html', {"msg": msg})
    type = request.GET['type']
    ss.type = type
    ss.save()
    msg = '成功！'
    return render(request, 'myadmin/prompt.html', {"msg": msg})


def myadmin_cource(request):
    cource = Course.objects.filter(is_active=True)
    return render(request, 'myadmin/myadimin_cource.html', {"cource": cource})


def myadmin_cource_del(request, id):
    try:
        ss = Course.objects.get(id=id)
    except Exception as e:
        print("cource_edit:", e)
        msg = "没有此课程！"
        return render(request, 'myadmin/prompt.html', {"msg": msg})
    ss.is_active = False
    ss.save()
    msg = "成功！"
    return render(request, 'myadmin/prompt.html', {"msg": msg})


def myadmin_cource_audit(request):
    try:
        cource = Course.objects.filter(is_active=True, audit=1)
    except Exception as e:
        print("cource_edit:", e)
        msg = "没有此课程！"
        return render(request, 'myadmin/prompt.html', {"msg": msg})
    return render(request, 'myadmin/cource_audit.html', {"cource": cource})


def myadmin_cource_audit_id(request, id):
    try:
        cource = Course.objects.get(id=id)
    except Exception as e:
        print("cource_edit:", e)
        msg = "没有此课程！"
        return render(request, 'myadmin/prompt.html', {"msg": msg})
    if request.method == 'GET':
        return render(request, 'myadmin/cource_audit_id.html', {"cource": cource})
    if request.method == 'POST':
        audit = request.POST['audit']
        cource.audit = audit
        cource.save()
        return HttpResponseRedirect('/myadmin/cource_audit/')


def myadmin_cource2(request):
    try:
        cource2 = Course2.objects.filter(is_active=True)
    except Exception as e:
        print("myadmin_cource2")
    return render(request, 'myadmin/cource2.html', {"cource2": cource2})
