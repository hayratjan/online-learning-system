from django.apps import AppConfig


class LoginConfig(AppConfig):
    name = 'login'
    verbose_name = '在线学习系统'
    verbose_name_plural = verbose_name