from django.shortcuts import render, HttpResponseRedirect, HttpResponse
from login.models import *
import hashlib


# Create your views here.
def index(request):
    try:
        cource = Course.objects.filter(is_active=True, audit=2)
    except Exception as e:
        print("login_index")
    return render(request, 'login/index.html', {"cource": cource})


def login(request):
    if request.method == 'GET':
        return render(request, 'login/login.html')
    elif request.method == 'POST':
        name = request.POST['name']
        pswd = request.POST['pswd']

        # 哈希算法
        m = hashlib.md5()
        m.update(pswd.encode())
        pswd = m.hexdigest()

        ss = User.objects.filter(name=name, password=pswd)
        if ss:
            msg = "登录成功！"
            user = User.objects.get(name=name).type
            request.session['name'] = name
            request.session['type'] = user
            return HttpResponseRedirect('/students/')

        else:
            msg = '请输入一个正确的用户名和密码,注意他们都是区分大小写的!'
            return render(request, 'login/login.html', {"msg": msg})


def register(request):
    if request.method == 'GET':
        return render(request, 'login/register.html')
    elif request.method == 'POST':
        name = request.POST['name']
        pswd = request.POST['pswd']
        pswd2 = request.POST['pswd2']
        if pswd == pswd2:
            ss = User.objects.filter(name=name)
            if ss:
                msg = "用户已存在！"
                return render(request, 'login/register.html', {"msg": msg})
            else:
                # 哈希算法
                m = hashlib.md5()
                m.update(pswd.encode())
                pswd = m.hexdigest()

                a = User.objects.create(name=name, password=pswd)
                msg = "注册成功！"

                return render(request, 'login/login.html', {"msg": msg})
        else:
            msg = "密码不一致"
            return render(request, "login/register.html", {"msg": msg})
    return render(request, 'login/register.html')


def logout(request):
    if 'name' in request.session:
        del request.session['name']
    return HttpResponseRedirect('/')


def teachers_register(request):
    if request.method == 'GET':
        return render(request, 'login/teachers_register.html')
    elif request.method == 'POST':
        id = request.POST['id']
        name = request.POST['name']

        nickname = request.session['name']
        # photo = request.FILES.get("photo")
        photo = request.FILES.get('photo')

        ss = User.objects.get(name=nickname)
        if ss:
            ss.teacher_id = id
            ss.teacher_name = name
            ss.photo = photo
            ss.type = 4
            ss.save()
            return HttpResponse('成功提交！')
        return HttpResponse('提交失败！')


def pswd_edit(request):
    user = request.session.get('name')
    if user:
        if request.method == 'GET':
            return render(request, 'login/pswd_edit.html')
        elif request.method == 'POST':
            pswd_1 = request.POST['pswd_1']
            pswd_2 = request.POST['pswd_2']
            pswd_3 = request.POST['pswd_3']
            # print(pswd_1,pswd_2,pswd_3)
            if pswd_2 != pswd_3:
                msg = "输入新密码不一致！！！"
                return render(request, 'login/pswd_edit.html', {"msg": msg})
            else:

                # 哈希算法
                m = hashlib.md5()
                m.update(pswd_1.encode())
                pswd = m.hexdigest()

                ss = User.objects.filter(name=user, password=pswd)
                if ss:
                    msg = "修改密码成功！！！"
                    stu = User.objects.get(name=user)
                    # 哈希算法
                    m = hashlib.md5()
                    m.update(pswd_2.encode())
                    pswd = m.hexdigest()
                    stu.password = pswd
                    stu.save()
                    if 'name' in request.session:
                        del request.session['name']
                    return render(request, 'login/login.html', {"msg": msg})


                else:
                    msg = '原密码错误！！！'
                    return render(request, 'login/pswd_edit.html', {"msg": msg})
    else:
        return HttpResponseRedirect('/login/')


def user_edit(request):
    name = request.session.get('name')
    try:
        user = User.objects.get(name=name, is_active=True)
    except Exception as e:
        print('user_edit:', e)
    if request.method == 'GET':
        return render(request, 'login/user_edit.html', {"user": user})
    if request.method == 'POST':
        phone = request.POST['email']
        email = request.POST['phone']
        user.phone = phone
        user.email = email
        user.save()

        return HttpResponseRedirect('/')

    return None
