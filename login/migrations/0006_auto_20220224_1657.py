# Generated by Django 2.2.12 on 2022-02-24 08:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('login', '0005_answer2_answer'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='answer2',
            name='answer',
        ),
        migrations.AddField(
            model_name='answer2',
            name='answer',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='login.Answer', verbose_name='问题'),
        ),
    ]
