import datetime

from django.db import models


# students
class User(models.Model):
    name = models.CharField(verbose_name="用户名", max_length=32, default='')  #
    teacher_id = models.CharField(verbose_name="id", max_length=32, default='')  #
    teacher_name = models.CharField(verbose_name="姓名", max_length=32, default='')  #
    type = models.IntegerField(verbose_name="类型", default=1)  # 1普通用户|3教工号|4审核状态|5拒绝|6管理员|9禁用
    password = models.CharField(verbose_name="密码", max_length=32, default='')  # 管理员密码
    Class = models.CharField(verbose_name="班级", max_length=32, default='')
    academy = models.CharField(verbose_name="学院", max_length=32, default='')
    email = models.EmailField(verbose_name="邮箱", default='')
    phone = models.CharField(verbose_name="手机号", max_length=18, default='')
    myTime = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)
    photo = models.ImageField(verbose_name="头像", upload_to='static/students/photo/', default='')
    is_active = models.BooleanField('是否活跃', default=True)

    def __str__(self):
        return self.name

    class Meta:
        # 数据库列表名
        db_table = 'User'
        # 后台管理名
        verbose_name_plural = '用户列表'


# 课程名
class Course(models.Model):
    name = models.CharField(verbose_name="课程名", max_length=32, default='')  # 课程名
    type = models.IntegerField(verbose_name="课程类型", default=0)
    # 1.哲学|2.经济学|3.法学|4.教育学|5.文学
    user = models.ForeignKey(verbose_name="用户名", to="User", on_delete=models.CASCADE, default='')  # 创建用户名
    photo = models.FileField(verbose_name="资源存放名字", upload_to='Course/', default='')  # 课程图片
    introduction = models.TextField(verbose_name="课程简介")  # 课程简介
    audit = models.IntegerField(verbose_name="审核", default=1)  # 审核状态 1审核中|   2已审核|   3审核失败
    myTime = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)

    is_active = models.BooleanField('是否活跃', default=True)

    def __str__(self):
        return self.name

    class Meta:
        # 数据库列表名
        db_table = 'Course'
        # 后台管理名
        verbose_name_plural = '课程'


# 章
class Chapter(models.Model):
    name = models.ForeignKey(to="Course", on_delete=models.CASCADE, default='', null=True)  # 课程名
    chapter = models.CharField(verbose_name="章", max_length=210, default="")  # 章
    is_active = models.BooleanField('是否活跃', default=True)

    def __str__(self):
        return self.name

    class Meta:
        # 数据库列表名
        db_table = 'Chapter'
        # 后台管理名
        verbose_name_plural = '章'


# pdf,mp4资源
class Media(models.Model):
    type = models.IntegerField(verbose_name="类型", default=0)  # 1.pdf| 2.mp4   |3.word | 4.powerPoint
    media = models.FileField(verbose_name="资源存放地址", upload_to='static/Media/', default='')
    is_active = models.BooleanField('是否活跃', default=True)

    def __str__(self):
        return self.media

    class Meta:
        # 数据库列表名
        db_table = 'Media'
        # 后台管理名
        verbose_name_plural = '资源存放地址'


# 节
class Section(models.Model):
    name = models.ForeignKey(to="Chapter", on_delete=models.CASCADE, default='')  # 章
    section = models.CharField(verbose_name="节", max_length=210, default="")
    media = models.ForeignKey(Media, on_delete=models.CASCADE)

    is_active = models.BooleanField('是否活跃', default=True)

    def __str__(self):
        return self.name

    class Meta:
        # 数据库列表名
        db_table = 'Section'
        # 后台管理名
        verbose_name_plural = '节'


# message留言
class Message(models.Model):
    name = models.ForeignKey(to="Section", on_delete=models.CASCADE, default='')  # 节
    user = models.ForeignKey(to="User", on_delete=models.CASCADE, default='')
    message = models.CharField(verbose_name="留言", max_length=210, default='')  # 留言
    myTime = models.DateTimeField(verbose_name="参加时间", auto_now_add=True)

    is_active = models.BooleanField('是否活跃', default=True)

    def __str__(self):
        return self.name

    class Meta:
        # 数据库列表名
        db_table = 'Message'
        # 后台管理名
        verbose_name_plural = '留言'


# 选课
class Course2(models.Model):
    students = models.ForeignKey(to="User", on_delete=models.CASCADE, default='')  # 用户名
    course = models.ForeignKey(to="Course", on_delete=models.CASCADE, default='')  # 课程名
    myTime = models.DateTimeField(verbose_name="参加时间", auto_now_add=True)
    is_active = models.BooleanField('是否活跃', default=True)

    def __str__(self):
        return self.course

    class Meta:
        # 数据库列表名
        db_table = 'course2'
        # 后台管理名
        verbose_name_plural = '选课'


# 练习、习题
class Exercises(models.Model):
    exercises = models.CharField(verbose_name="练习题", max_length=210, default='')
    course = models.ForeignKey(verbose_name='课程', to="Course", on_delete=models.CASCADE, default='')
    myTime = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)
    is_active = models.BooleanField('是否活跃', default=True)

    def __str__(self):
        return self.exercises

    class Meta:
        # 数据库列表名
        db_table = 'Exercises'
        # 后台管理名
        verbose_name_plural = '习题'


class Exercises2(models.Model):
    user = models.ForeignKey(verbose_name="用户", to="User", on_delete=models.CASCADE, default='')
    exercises = models.ForeignKey(verbose_name="联系题名", to="Exercises", on_delete=models.CASCADE, default='')
    course2 = models.ForeignKey(verbose_name='课程', to="Course2", on_delete=models.CASCADE, default='')
    grade = models.DecimalField(verbose_name="成绩", max_digits=5, decimal_places=2)
    myTime = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)
    is_active = models.BooleanField('是否活跃', default=True)

    def __str__(self):
        grade = str(self.grade)
        return grade

    class Meta:
        # 数据库列表名
        db_table = 'Exercises2'
        # 后台管理名
        verbose_name_plural = '习题成绩'


# 选择题
class Answer(models.Model):
    exercises = models.ForeignKey(to="Exercises", on_delete=models.CASCADE, default='')
    title = models.TextField(verbose_name="问题", max_length=210, default='')
    answerA = models.CharField(verbose_name="答案A", max_length=110, default='')
    answerB = models.CharField(verbose_name="答案B", max_length=110, default='')
    answerC = models.CharField(verbose_name="答案C", max_length=110, default='')
    answerD = models.CharField(verbose_name="答案D", max_length=110, default='')
    correctAnswer = models.IntegerField(verbose_name="正确答案", default=0)  # A B C D ___ 1 2 3 4
    number = models.IntegerField(verbose_name="分值", default=2)
    is_active = models.BooleanField('是否活跃', default=True)

    def __str__(self):
        return self.title

    class Meta:
        # 数据库列表名
        db_table = 'Answer'
        # 后台管理名
        verbose_name_plural = '选择题'


class Answer2(models.Model):
    user = models.ForeignKey(verbose_name="学生", to="User", on_delete=models.CASCADE, default="")
    exercises = models.ForeignKey(verbose_name="练习题", to="Exercises", on_delete=models.CASCADE, default="")
    answer = models.ForeignKey(verbose_name="问题", to="Answer", on_delete=models.CASCADE, default='')
    stuAnswer = models.IntegerField(verbose_name="学生答案", default=0)
    is_active = models.BooleanField('是否活跃', default=True)

    def __str__(self):
        answer = str(self.stuAnswer)
        return answer

    class Meta:
        # 数据库列表名
        db_table = 'Answer2'
        # 后台管理名
        verbose_name_plural = '学生答案'


class Teachers(models.Model):
    nickName = models.CharField(verbose_name="用户名", max_length=110, default='')
    name = models.CharField(verbose_name="姓名", max_length=110, default='')
    type = models.IntegerField(verbose_name="类型", default=1)  # 1普通用户|6管理员|9禁用
    password = models.CharField(verbose_name="密码", max_length=32, default='')  # 管理员密码
    email = models.EmailField(verbose_name="邮箱", default='')
    phone = models.CharField(verbose_name="手机号", max_length=11, default='')
    photo = models.ImageField(verbose_name="头像", upload_to='teachers/photo/', default='')
    myTime = models.DateTimeField(verbose_name="参加时间", auto_now_add=True)
    is_active = models.BooleanField('是否活跃', default=True)


class MyTime(models.Model):
    myTime = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=110, default='')
