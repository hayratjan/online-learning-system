from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index),
    path('user_edit/', views.user_edit, name='user_edit'),
    path('login/', views.login, name='login'),
    path('register/', views.register, name="register"),
    path('teachers_register/', views.teachers_register, name="teachers_register"),
    path('logout/', views.logout, name='logout'),
    path('pswd_edit/', views.pswd_edit, name='pswd_edit'),

]
