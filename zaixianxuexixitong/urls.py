from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('', include('login.urls')),
                  path('teachers/', include('teachers.urls')),
                  path('students/', include('students.urls')),
                  path('myadmin/', include('myadmin.urls')),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)  ## 没有这一句无法显示上传的图片
