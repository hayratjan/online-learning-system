from django.shortcuts import render, HttpResponseRedirect

from login.models import *


def index(request):
    user = request.session.get('name')
    user = User.objects.get(name=user)
    ss = Course.objects.filter(user=user, is_active=True)
    return render(request, 'teachers/index.html', {"cource": ss})


def cource_add(request):
    if request.method == 'GET':
        return render(request, 'teachers/cource_add.html')
    elif request.method == 'POST':
        name = request.POST['name']
        # photo = request.POST['name']
        photo = request.FILES.get('photo')
        introduction = request.POST['introduction']
        type = request.POST['type']
        user = request.session.get('name')
        user = User.objects.get(name=user)
        ss = Course.objects.create(name=name, photo=photo, introduction=introduction, type=type, user=user)
        return HttpResponseRedirect('/teachers/')


def cource_edit(request, id):
    try:

        cource = Course.objects.get(id=id, is_active=True)

    except Exception as e:
        print("cource_edit:", e)
        msg = "没有此课程！"
        return render(request, 'teachers/prompt.html', {"msg": msg})

    chapter_id = request.GET.get('chapter')
    section_id = request.GET.get('section')

    chapter = Chapter.objects.filter(name__id=id, is_active=True)
    section = Section.objects.filter(name__name_id=id, is_active=True)

    if section_id:
        try:
            section_id = Section.objects.get(id=section_id, is_active=True)
            message = Message.objects.filter(name_id=section_id, is_active=True)
        except Exception as e:
            print("cource_edit section_id：", e)
            msg = "没有此课程！"
        if section_id.media.type == 3:
            return render(request, 'teachers/cource_edit.html',
                          {"cource": cource, "chapter": chapter, "section": section, "id": id, "section_id": section_id,
                           "message": message, "chapter_id": chapter_id})
            # return render(request, 'teachers/prompt.html', {"msg": msg})

        return render(request, 'teachers/cource_edit.html',
                      {"cource": cource, "chapter": chapter, "section": section, "id": id, "section_id": section_id,
                       "message": message, "chapter_id": chapter_id})

    return render(request, 'teachers/cource_edit.html',
                  {"cource": cource, "chapter": chapter, "section": section, "id": id})


def cource_del(request, id):
    try:
        ss = Course.objects.get(id=id)
    except Exception as e:
        print("cource_edit:", e)
        msg = "没有此课程！"
        return render(request, 'teachers/prompt.html', {"msg": msg})
    ss.is_active = False
    ss.save()
    msg = "成功！"
    return render(request, 'teachers/prompt.html', {"msg": msg})


def cource_chapter_add(request, id):
    try:
        cource = Course.objects.get(id=id)
    except Exception as e:
        print("cource_edit:", e)
        msg = "没有此课程！"
    if request.method == 'POST':
        name = request.POST['name']
        ss = Chapter.objects.create(name=Course.objects.get(id=id), chapter=name)

        return HttpResponseRedirect('/teachers/cource_edit/' + str(id))

    return HttpResponseRedirect('/teachers/cource_edit/' + str(id))


def cource_chapter_edit(request, id, chapter_id):
    try:
        cource = Course.objects.get(id=id, is_active=True)
        chapter = Chapter.objects.get(id=chapter_id, is_active=True)
    except Exception as e:
        print("cource_edit:", e)
        msg = "没有此课程或目录！"
    if request.method == 'POST':
        name = request.POST['name']
        ss = Chapter.objects.get(name=Course.objects.get(id=id), id=chapter_id)
        ss.chapter = name
        ss.save()

        return HttpResponseRedirect('/teachers/cource_edit/' + str(id))

    return HttpResponseRedirect('/teachers/cource_edit/' + str(id))


def cource_chapter_del(request, id, chapter_id):
    try:
        cource = Course.objects.get(id=id)
        chapter = Chapter.objects.get(id=chapter_id)
    except Exception as e:
        print("cource_del:", e)
        msg = "没有此课程或目录！"
    if request.method == 'GET':
        chapter.is_active = False
        chapter.save()
        return HttpResponseRedirect('/teachers/cource_edit/' + str(id))


def cource_section_add(request, id, chapter_id):
    """
    收到的信息
        1.节名
        2.关联章的id地址
        3.media资源和类型

    返回的内容
        1.跳转到课程编辑页面
            课程id

    """
    chapter_id = chapter_id
    try:
        cource = Course.objects.get(id=id)
        chapter = Chapter.objects.get(id=chapter_id)
    except Exception as e:
        print("cource_edit:", e)
        msg = "没有此课程或目录！"
    if request.method == 'POST':
        name = request.POST['name']
        type = request.POST['type']
        media = request.FILES.get('file')
        ss = Section.objects.create(name=chapter, section=name, media=Media.objects.create(type=type, media=media))
        return HttpResponseRedirect('/teachers/cource_edit/' + str(id), {"section_id": ss})


# 添加评论
def cource_section_msg(request, id, chapter_id, section_id):
    try:
        cource = Course.objects.get(id=id)
        chapter = Chapter.objects.get(id=chapter_id)
    except Exception as e:
        print("cource_edit:", e)
        msg = "没有此课程或目录！"
    if request.method == 'GET':
        msg = request.GET.get('msg')
        user = request.session.get('name')

        ss = Message.objects.create(message=msg, user=User.objects.get(name=user),
                                    name=Section.objects.get(id=section_id))
        return HttpResponseRedirect('/teachers/cource_edit/' + str(id))


def cource_section_edit(request, id, chapter_id, section_id):
    """
    收到的信息
        1.节名
        2.关联章的id地址
        3.media资源和类型

    返回的内容
        1.跳转到课程编辑页面
            课程id

    """
    try:
        # cource = Course.objects.get(id=id)
        # chapter = Chapter.objects.get(id=chapter_id)
        section = Section.objects.get(id=section_id)
        media = Media.objects.get(id=section.media.id)
    except Exception as e:
        print("cource_edit:", e)
        msg = "没有此课程或目录！"
        return render(request, 'teachers/prompt.html', {"msg": msg})
    if request.method == 'POST':
        name = request.POST['name']
        type = request.POST['type']
        file = request.FILES.get('file')
        section.section = name
        section.save()
        media.type = type
        media.media = file
        media.save()

        return HttpResponseRedirect('/teachers/cource_edit/' + str(id))


def cource_section_del(request, id, chapter_id, section_id):
    try:
        cource = Course.objects.get(id=id)
        chapter = Chapter.objects.get(id=chapter_id)
        section = Section.objects.get(id=section_id)
    except Exception as e:
        print("cource_edit:", e)
        msg = "没有此课程或目录！"
        return render(request, 'teachers/prompt.html', {"msg": msg})
    section.is_active = False
    section.save()
    return HttpResponseRedirect('/teachers/cource_edit/' + str(id))


def exercises(request, id):
    try:
        course = Course.objects.get(id=id)
        exercises = Exercises.objects.filter(course_id=id, is_active=True)
        ans = Answer.objects.filter(is_active=True)
    except Exception as e:
        print("teacher_exercises:", e)
        msg = "没有此课程，刷新再查看！"
        return render(request, 'teachers/prompt.html', {"msg": msg})
    if request.method == 'GET':
        return render(request, 'teachers/teachers_exercises.html', {"course": course, "exercises": exercises,"ans":ans})


def exercises_add(request, id):
    try:
        course = Course.objects.get(id=id)
    except Exception as e:
        print("teacher_exercises:", e)
        msg = "没有此课程，刷新再查看！"
        return render(request, 'teachers/prompt.html', {"msg": msg})
    if request.method == 'GET':
        name = request.GET['name']
        ss = Exercises.objects.create(exercises=name, course=course)
        return HttpResponseRedirect('/teachers/exercises/%s' % id)


def exercises_del(request, id, exercises_id):
    try:
        exercises = Exercises.objects.get(id=exercises_id)
    except Exception as e:
        print("teachers_exercises_del:", exercises)
        msg = "没有此课程，刷新再查看！"
        return render(request, 'teachers/prompt.html', {"msg": msg})
    exercises.is_active = False
    exercises.save()
    return HttpResponseRedirect('/teachers/exercises/%s' % id)


def exercises_edit(request, exercise_id):
    try:
        exercises = Exercises.objects.get(id=exercise_id, is_active=True)
        ansewer = Answer.objects.filter(exercises_id=exercise_id, is_active=True)
    except Exception as e:
        msg = "没有此练习题！！！"
        print("exercises_edit:", e)
        return render(request, 'teachers/prompt.html', {"msg": msg})
    if request.method == 'GET':
        return render(request, 'teachers/exercises_edit.html', {"exercises": exercises, "ansewer": ansewer})
    return None


def ansewer_add(request, exercise_id):
    try:
        exercises = Exercises.objects.get(id=exercise_id)
        ansewer = Answer.objects.filter(exercises_id=exercise_id)
    except Exception as e:
        msg = "没有此练习题！！！"
        print("exercises_edit:", e)
        return render(request, 'teachers/prompt.html', {"msg": msg})
    if request.method == 'POST':
        title = request.POST['title']
        a = request.POST['a']
        b = request.POST['b']
        c = request.POST['c']
        d = request.POST['d']
        e = request.POST['e']
        ss = Answer.objects.create(exercises=Exercises.objects.get(id=exercise_id),
                                   title=title,
                                   answerA=a,
                                   answerB=b,
                                   answerC=c,
                                   answerD=d, correctAnswer=int(e))
    return HttpResponseRedirect('/teachers/exercises_edit/' + str(exercise_id))


def ansewer_edit(request, exercise_id, ansewer_id):
    try:
        exercises = Exercises.objects.get(id=exercise_id)
        ansewer = Answer.objects.get(id=ansewer_id)

    except Exception as e:
        msg = "没有此练习题！！！"
        print("exercises_edit:", e)
        return render(request, 'teachers/prompt.html', {"msg": msg})
    if request.method == 'POST':
        title = request.POST['title']
        a = request.POST['a']
        b = request.POST['b']
        c = request.POST['c']
        d = request.POST['d']
        e = request.POST['e']
        ansewer.title = title
        ansewer.answerA = a
        ansewer.answerB = b
        ansewer.answerC = c
        ansewer.answerD = d
        ansewer.save()
    return HttpResponseRedirect('/teachers/exercises_edit/' + str(exercise_id))


def ansewer_del(request, exercise_id, ansewer_id):
    try:
        exercises = Exercises.objects.get(id=exercise_id)
        ansewer = Answer.objects.get(id=ansewer_id)

    except Exception as e:
        msg = "没有此练习题！！！"
        print("exercises_edit:", e)
        return render(request, 'teachers/prompt.html', {"msg": msg})
    if request.method == 'GET':
        ansewer.is_active = False
        ansewer.save()
    return HttpResponseRedirect('/teachers/exercises_edit/' + str(exercise_id))
