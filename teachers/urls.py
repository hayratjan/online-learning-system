from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name="teachers_index"),  # 教师端首页
    path('cource_add/', views.cource_add, name="cource_add"),  #
    path('cource_edit/<int:id>', views.cource_edit, name="cource_edit"),  #
    path('cource_del/<int:id>', views.cource_del, name="cource_del"),  # 课程删除
    path('cource_chapter_add/<int:id>', views.cource_chapter_add, name="cource_chapter_add"),  # 添加章
    path('cource_chapter_edit/<int:id>/<int:chapter_id>', views.cource_chapter_edit, name="cource_chapter_edit"),  # 编辑章
    path('cource_chapter_del/<int:id>/<int:chapter_id>', views.cource_chapter_del, name="cource_chapter_del"),  # 删除章
    path('cource_section_add/<int:id>/<int:chapter_id>', views.cource_section_add, name="cource_section_add"),  # 添加节
    path('cource_section_edit/<int:id>/<int:chapter_id>/<int:section_id>', views.cource_section_edit,
         name="cource_section_edit"),  # 添加节
    path('cource_section_del/<int:id>/<int:chapter_id>/<int:section_id>', views.cource_section_del,
         name="cource_section_del"),  # 添加节
    path('cource_section_msg/<int:id>/<int:chapter_id>/<int:section_id>', views.cource_section_msg,
         name="cource_section_msg"),  # 课程评论留言
    # 练习题
    path('exercises/<int:id>', views.exercises, name="exercises"),  # 练习题
    path('exercises_add/<int:id>', views.exercises_add, name="exercises_add"),  # 练习题添加
    path('exercises_del/<int:id>/<int:exercises_id>', views.exercises_del, name="exercises_del"),  # 练习题删除
    path('exercises_edit/<int:exercise_id>', views.exercises_edit, name="exercises_edit"),  # 练习题编辑
    path('ansewer_add/<int:exercise_id>', views.ansewer_add, name="ansewer_add"),  # 添加题
    path('ansewer_edit/<int:exercise_id>/<ansewer_id>', views.ansewer_edit, name="ansewer_edit"),  # 修改题
    path('ansewer_del/<int:exercise_id>/<ansewer_id>', views.ansewer_del, name="ansewer_del"),  # 修改题


]
